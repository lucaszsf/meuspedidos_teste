from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from vagaDev.views import *


urlpatterns = [
    url(r'^admin/', admin.site.urls),
	url(r'^vagadev/$', vagaDevView.as_view(), name='vaga-dev')
]
