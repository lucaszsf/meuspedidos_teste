from django.shortcuts import render, redirect
from django.views.generic.base import View
from vagaDev.forms.candidato import CandidatoForm
from django.contrib import messages
from vagaDev.models.candidato import Candidato
from django.core.mail import EmailMessage

def mensagem_email(candidato):

	assunto_email = 'Obrigado por se candidatar!'
	cargos_passou = []

	if (int(candidato.nivel_html) and int(candidato.nivel_css) and int(candidato.nivel_js)) >= 7:
		cargos_passou.append('programador Front-End')
		
	if (int(candidato.nivel_python) and int(candidato.nivel_django)) >= 7:
		cargos_passou.append('programador Back-End')

	if (int(candidato.nivel_ios) or int(candidato.nivel_android)) >= 7:
		cargos_passou.append('programador Mobile')

	if len(cargos_passou) == 0:
		cargos_passou.append('programador')

	for cargo in cargos_passou:
		email = EmailMessage(
			assunto_email, 
			'Obrigado por se candidatar, assim que tivermos uma vaga disponivel para ' + cargo + ' entraremos em contato.',  
			to=[candidato.email]
			)
		email.send()

class vagaDevView(View):

	template = 'vagadev/salvar.html'

	def get(self, request):
		form = CandidatoForm()
		return render(request, self.template, {'form': form})

	def post(self, request):
		form = CandidatoForm(request.POST)

		check_email = Candidato.objects.filter(email=request.POST['email'])
		
		if form.is_valid() and len(check_email) == 0:
			post = form.save(commit=False)
			post.nome = request.POST['nome']
			post.email = request.POST['email']
			post.nivel_html = request.POST['nivel_html']
			post.nivel_css = request.POST['nivel_css']
			post.nivel_js = request.POST['nivel_js']
			post.nivel_ptyhon = request.POST['nivel_python']
			post.nivel_django = request.POST['nivel_django']
			post.nivel_ios = request.POST['nivel_ios']
			post.nivel_android = request.POST['nivel_android']
			post.save()
			messages.success(request, 'Candidato cadastrado com sucesso! Em alguns minutos voce recebera um e-mail com a vaga correspondente.')
			mensagem_email(post)
	 		return redirect('/vagadev/')
	 	else:
	 		if len(check_email) > 0:
	 			messages.error(request, 'Email ja cadastrado! Por favor informe outro email.')

			return render(request, self.template, {'form': form})