from __future__ import unicode_literals
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Candidato(models.Model):
	nome = models.CharField(max_length=50)
	email = models.EmailField(max_length=254)
	nivel_html = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)
	nivel_css = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)
	nivel_js = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)
	nivel_python = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)
	nivel_django = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)
	nivel_ios = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)
	nivel_android = models.PositiveIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)], null=True, default=0)