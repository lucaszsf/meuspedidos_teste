from django import forms
from vagaDev.models import Candidato

class CandidatoForm(forms.ModelForm):

	class Meta:
		model = Candidato
		fields = '__all__'
